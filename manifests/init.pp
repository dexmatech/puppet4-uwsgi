class uwsgi (
    $package_name        = $uwsgi::params::package_name,
    $package_ensure      = $uwsgi::params::package_ensure,
    $package_provider    = $uwsgi::params::package_provider,
    $service_name        = $uwsgi::params::service_name,
    $service_file        = undef,
    $service_file_mode   = undef,
    $service_template    = undef,
    $service_ensure      = $uwsgi::params::service_ensure,
    $service_enable      = $uwsgi::params::service_enable,
    $service_provider    = $uwsgi::params::service_provider,
    $manage_service_file = $uwsgi::params::manage_service_file,
    $binary_directory    = $uwsgi::params::binary_directory,
    $config_file         = $uwsgi::params::config_file,
    $log_file            = $uwsgi::params::log_file,
    $log_rotate          = $uwsgi::params::log_rotate,
    $app_directory       = $uwsgi::params::app_directory,
    $install_pip         = $uwsgi::params::install_pip,
    $install_python_dev  = $uwsgi::params::install_python_dev,
    $python_pip          = $uwsgi::params::python_pip,
    $python_dev          = $uwsgi::params::python_dev,
    $emperor_options     = undef
) inherits uwsgi::params {

    package { 'uwsgi':
      ensure    => present,
      provider  => 'pip',
    }

    $uwsgi_python_packages = [ 'uwsgi-plugin-python3', 'uwsgi-plugin-python']

    package { $uwsgi_python_packages: }

    service { 'uwsgi-emperor':
      ensure      => running,
      enable      => true,
      hasrestart  => true,
      hasstatus   => false,
    }

    package { 'python-simplejson':
      ensure      => present,
    }
    package { 'uwsgitop':
      ensure      => present,
      provider    => 'pip',
    }
    file { '/etc/default/uwsgi-emperor':
      ensure      => file,
      mode        => '0644',
      owner       => 'root',
      group       => 'root',
      content     => template('uwsgi/uwsgi-emperor.erb'),
    }

    file { '/etc/init.d/uwsgi-emperor':
      ensure      => file,
      mode        => '0755',
      owner       => 'root',
      group       => 'root',
      content     => template('uwsgi/sysv.uwsgi-emperor.erb'),
    }

    file { '/var/log/uwsgi':
      ensure      => directory,
      mode        => '0755',
      owner       => 'root',
      group       => 'root',
    }

    file { '/var/log/uwsgi/app':
      ensure      => directory,
      mode        => '0755',
      owner       => 'www-data',
      group       => 'www-data',
    }

    file { '/etc/uwsgi-emperor/emperor.ini':
      ensure      => file,
      mode        => '0644',
      owner       => 'root',
      group       => 'root',
      content     => template('uwsgi/emperor.ini.erb'),
      notify      => Service['uwsgi-emperor'],
    }

    $folders = [ '/etc/uwsgi-emperor/', '/etc/uwsgi-emperor/vassals', '/etc/uwsgi-emperor/reload']

    file { $folders:
      ensure      => directory,
      mode        => '0755',
      owner       => 'root',
      group       => 'root',
    }

    # finally, configure any applications necessary
    $applications = hiera_hash('uwsgi::app', {})
    create_resources('uwsgi::app', $applications)
}
