class uwsgi::params {
  $package_name         = 'uwsgi'
  $package_ensure       = 'installed'
  $package_provider     = 'pip'
  $service_name         = 'uwsgi'
  $service_enable       = true
  $service_ensure       = true
  $service_provider     = 'service'
  $manage_service_file  = true
  $binary_directory     = '/usr/local/bin/uwsgi'
  $config_file          = '/etc/uwsgi-emperor/emperor.ini'
  $install_pip          = true
  $install_python_dev   = true
  $log_file             = '/var/log/uwsgi/uwsgi-emperor.log'
  $log_rotate           = 'no'
  $python_pip           = 'python-pip'

  case $::osfamily {
        redhat: {
          $app_directory = '/etc/uwsgi.d'
          $pidfile       = '/var/run/uwsgi/uwsgi.pid'
          $python_dev    = 'python-devel'
          $socket        = '/var/run/uwsgi/uwsgi.socket'
        }
        default: {
          $app_directory = '/etc/uwsgi-emperor/vassals'
          $python_dev    = 'python-dev'
          $socket        = '/var/run/uwsgi/uwsgi.socket'
          $pidfile       = '/var/run/uwsgi/uwsgi.pid'


        }
  }
}
