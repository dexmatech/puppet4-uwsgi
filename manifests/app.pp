define uwsgi::app (
  $ensure               = 'present',
  $template             = 'uwsgi/app.ini.erb',
  $application_options  = undef,
  $uid                  = undef,
  $gid                  = undef
  ) {

  file { "${::uwsgi::app_directory}/${title}.ini":
    ensure  => $ensure,
    owner   => 'jenkins',
    group   => 'jenkins',
    mode    => '0644',
    content => template($template),
    require => Package[$::uwsgi::package_name]
    }
  
  #exec { 'chain-reload':
  #  command     => "touch ${::uwsgi::app_directory}/${title}.ini",
  #  path        => '/usr/bin',
  #  subscribe   => File["${::uwsgi::app_directory}/${title}.ini"],
  #  refreshonly => true,
  #  }
  }
